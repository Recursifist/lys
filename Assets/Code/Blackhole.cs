using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Lys.Interfaces;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Lys
{
    public class Blackhole : Killer, IPrefabMessages
    {
        private float size = 1f;
        private PointEffector2D grav;
        private ParticleSystem[] particleSystems;
        private System.Random rand = new System.Random();

        // Message Events

        public void SetSize(int _size) { Start(); GrowToSize(_size); }

        // Unity Events

        new void Start()
        {
            base.Start();
            grav = gameObject.GetComponentInChildren<PointEffector2D>();
            particleSystems = gameObject.GetComponentsInChildren<ParticleSystem>();

            var localRot = gameObject.transform.localRotation;
            gameObject.transform.localRotation = new Quaternion(localRot.x, localRot.y, (float)rand.Next(0, 45), localRot.w);
        }

        void Update()
        {
            if (DoneKilling)
            {
                grav.transform.parent.gameObject.SetActive(false);
                var mainCam = GameObject.FindObjectOfType<Camera>();
                ExecuteEvents.Execute<ICameraMessages>(mainCam.gameObject, null, (x, _) => x.Zoom(900f, 1.5f));
            }

            EventHorizonProximity();
        }

        // Methods

        void GrowToSize(float _size)
        {
            size = _size;

            // particle and hole
            Action<float> smoothingF = ratio =>
            {
                gameObject.transform.localScale = new Vector3()._SetSize((0.4f + (size * 0.5f)) * ratio);
                UpdateStrength();
            };
            Action finalF = () =>
            {
                gameObject.transform.localScale = new Vector3()._SetSize(0.4f + (size * 0.5f));
                UpdateStrength();
            };
            var growToSize = Utils.MakeCoroutine(20f, smoothingF, finalF);

            // jets
            var jets = particleSystems.Where(ps => ps.name.Contains("Jet")).ToList();
            Action<float> jetsSmoothingF = ratio =>
            {
                jets.ForEach(ps => ps.transform.localScale = new Vector3()._SetSize(ratio));
            };
            Action jetsFinalF = () =>
            {
                jets.ForEach(ps => ps.transform.localScale = new Vector3()._SetSize(1));
            };
            var growJets = Utils.MakeCoroutine(60f, jetsSmoothingF, jetsFinalF);

            StartCoroutine(growToSize);
            StartCoroutine(growJets);
        }

        void UpdateStrength()
        {
            foreach (var ps in particleSystems.Where(x => x.name.Contains("Disc")))
            {
                var main = ps.main;
                main.startLifetime = 2 + size;
            }
            grav.forceMagnitude = -200 * size;
        }

        void EventHorizonProximity()
        {
            var player = GameObject.Find("Player");
            var dist = Vector3.Distance(player.transform.position, gameObject.transform.position);
            var prox = 6;
            if (dist < prox)
            {
                var dFactor = dist < 3 ? 0.4f : 0.6f;
                Time.timeScale = dFactor;
            }
            else if (dist > prox)
            {
                Time.timeScale = 1;
            }
        }
    }
}
