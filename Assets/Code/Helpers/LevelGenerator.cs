using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Lys.Interfaces;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Lys
{
    public class LevelGenerator
    {
        private Scene scene;
        private GameObject player;
        private GameObject boundry;
        private GameObject prefabContainer;
        private GameObject antiparticlePrefab;
        private GameObject particlePrefab;
        private GameObject blackholePrefab;
        private static System.Random rand = new System.Random();

        public LevelGenerator(GameObject _player, GameObject _boundry,
            GameObject _antiparticlePrefab, GameObject _particlePrefab, GameObject _blackholePrefab)
        {
            scene = SceneManager.GetActiveScene();
            player = _player;
            boundry = _boundry;
            antiparticlePrefab = _antiparticlePrefab;
            particlePrefab = _particlePrefab;
            blackholePrefab = _blackholePrefab;

            prefabContainer = GameObject.Find("Generated");
        }

        private GameObject InitPrefab(GameObject prefab, float x, float y)
        {
            var gObj = GameObject.Instantiate(prefab, prefabContainer.transform);

            var deviation = (float)rand.NextDouble() * 2f;
            gObj.transform.position = new Vector3(x + deviation, y + deviation, 0);

            ExecuteEvents.Execute<IPlayerRelativeMessages>(gObj, null, (x, _) => x.SetPlayer(player));
            return gObj;
        }

        void CreateAnitparticle(float x, float y)
        {
            var gObj = InitPrefab(antiparticlePrefab, x, y);
        }

        void CreateParticle(float x, float y)
        {
            var gObj = InitPrefab(particlePrefab, x, y);
        }

        void CreateBlackhole(float x, float y)
        {
            var gObj = InitPrefab(blackholePrefab, x, y);
            ExecuteEvents.Execute<IPrefabMessages>(gObj, null, (x, _) => x.SetSize(rand.Next(1, 3)));
        }

        public void Clear()
        {
            var dList = GameObject.FindGameObjectsWithTag("Particle")
                .Concat(GameObject.FindGameObjectsWithTag("Antiparticle"))
                .Concat(GameObject.FindGameObjectsWithTag("Killer"));

            foreach (var go in dList) { UnityEngine.Object.Destroy(go); }
        }

        public void Gen(int complexity)
        {
            Clear();

            var radius = (10f * complexity) + 10;
            boundry.transform.localScale = new Vector3()._SetSize(radius);

            var energyTotal = 0;
            var apCount = 0;

            var bhCount = 0;
            var bhSpacing = 13;
            var bhSpacingX = bhSpacing;
            var bhSpacingY = bhSpacing;

            var safeZoneDiameter = 3;
            Func<double, bool> insideSafeZone = r => r > -safeZoneDiameter && r < safeZoneDiameter;

            var gridScale = 2;
            var x = -radius;
            var y = -radius;

            while (x < radius)
            {
                x += gridScale;
                if (insideSafeZone(x)) { continue; }

                bhSpacingX++;
                y = -radius;
                while (y < radius)
                {
                    y += gridScale;
                    bhSpacingY++;
                    if (insideSafeZone(y)) { continue; }

                    var density = Mathf.PerlinNoise(x / 10, y / 10) * 0.6f;
                    if (density < 0.3) { continue; }

                    switch (rand.Next(4))
                    {
                        case 1:
                            CreateParticle(x, y);
                            energyTotal++;
                            break;

                        case 2:
                            if (apCount / ++energyTotal < 0.5f)
                            {
                                if (rand.Next(0, complexity) > 3)
                                {
                                    CreateAnitparticle(x, y);
                                }
                                CreateAnitparticle(x, y);
                                apCount++;
                            }
                            else CreateParticle(x, y);
                            break;

                        case 3:
                            var isSpaced = (bhSpacingX >= bhSpacing) && (bhSpacingY >= bhSpacing);
                            var underLimit = bhCount < (complexity - 1) * 2;
                            if (isSpaced && underLimit)
                            {
                                CreateBlackhole(x, y);
                                bhSpacingX = 0;
                                bhSpacingY = 0;
                                bhCount++;
                            }
                            break;
                        default: break;
                    }
                }
            }
        }

    }
}
