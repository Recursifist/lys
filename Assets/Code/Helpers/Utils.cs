using System.Threading.Tasks;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Lys
{
    /// Extention Methods use '_' for clarity
    public static class Utils
    {

        // Gameobject
        public static T _GetGameObjectByName<T>(this GameObject gObj, String name) where T : UnityEngine.Object
        {
            return gObj.GetComponentsInChildren<T>().First(o => o.name == "Edge");
        }

        public static bool _CompareTags(this GameObject gObj, String[] tags)
        {
            if (tags.Length < 1) return false;
            else return Array.Exists(tags, t => gObj.CompareTag(t ?? "null"));
        }

        // Input
        private static Func<KeyCode, bool> gk = k => Input.GetKey(k);
        private static Func<KeyCode, bool> gkd = k => Input.GetKeyDown(k);
        public static bool _Pressed(this KeyCode key) { return gkd(key); }
        public static bool _PressedWith(this KeyCode key, KeyCode withKey) { return gkd(key) && gk(withKey); }
        public static bool _PressedOr(this KeyCode key, KeyCode orKey) { return gkd(key) || gkd(orKey); }

        // Vector
        public static Vector2 _SetSize(this Vector2 vec, float size) { return new Vector2(size, size); }
        public static Vector3 _SetSize(this Vector3 vec, float size) { return new Vector3(size, size, size); }

        // Subroutine
        public static IEnumerator MakeCoroutine(float duration, Action<float> smoothingAction, Action finalAction)
        {
            var elapsed = 0f;
            while (elapsed < duration)
            {
                var ratio = elapsed / duration;
                smoothingAction(ratio);

                elapsed += Time.deltaTime;
                yield return null;
            }

            if (finalAction != null) finalAction();
        }

        public static IEnumerator MakeDelayCoroutine(float delay, Action action)
        {
            return MakeCoroutine(delay, _ => { return; }, action);
        }
    }
}
