using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

namespace Lys
{
    public class PlayerSizeHelper
    {
        public float S; //size
        public int Energy;
        public const float Load = Min / 10;
        public const float Min = 0.2f;
        public const float Max = 1f;
        public Vector2 Dimensions;
        public float Speed;
        public float FTLSpeed;
        public float TrailWidth;
        public float Zoom;

        private float dimensionFactor = 0.2f;
        private float speedFactor = 5f;
        private float ftlFactor = 2200f;
        private float trailWidthFactor = 2f;
        private float zoomFactor = 0.5f;

        public PlayerSizeHelper(float _size = Min)
        {
            S = _size < Min ? Min : _size;
            CalculateProps();
        }

        public float GetCurrentSize() => S;
        public void ChangeSize(float size)
        {
            if (size < Min) { size = Min; }
            if (size > Max) { size = Max; }

            S = size;
            CalculateProps();
        }

        private void CalculateProps()
        {
            Energy = (int)Math.Round((S - Min) / Load, 0);
            var sFactor = 1 + S;
            Dimensions = new Vector2()._SetSize(sFactor * dimensionFactor);
            TrailWidth = trailWidthFactor * sFactor;
            Speed = speedFactor * sFactor;
            FTLSpeed = ftlFactor * sFactor;
            Zoom = 6 + (zoomFactor * Energy);
        }

    }
}