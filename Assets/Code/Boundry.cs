using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Lys.Interfaces;

namespace Lys
{
    public class Boundry : MonoBehaviour
    {
        // Unity Events

        void Update()
        {
            Expand();
        }

        void OnTriggerExit2D(Collider2D other)
        {
            WarpToOppositeSide(other);
        }

        // Methods

        void Expand() {
            gameObject.transform.localScale = new Vector3()._SetSize(gameObject.transform.localScale.x + 0.001f);
            gameObject.transform.Rotate(0.0f, 0.0f, 0.05f, Space.World);
        }

        void WarpToOppositeSide(Collider2D other)
        {
            var pos = other.gameObject.transform.position;
            other.gameObject.transform.position = new Vector3(-pos.x, -pos.y, pos.z);
        }
    }
}
