using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Lys.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Lys
{
    public class UIManager : MonoBehaviour, IUIMessages
    {
        public GameObject GameManager;
        public GameObject Player;

        private AudioSource music;
        private Text menuText;
        private Text startText;
        private Text endText;
        private GameObject bsod;
        private GameObject background;
        private GameObject hud;
        private Text pointsText;
        private Text ftlText;
        private Text shootText;
        private Text levelText;
        private Text levelCenterText;
        private float NormalMusicPitch;

        // Message Events
        public void ShowStart() { ShowStartScreen(); }
        public void ShowEnd() { ShowEndScreenScreen(); }
        public void EnableBSOD() { BSOD_On(); }
        public void DisableBSOD() { BSOD_Off(); }
        public void EnableHUD() { HUD_On(); }
        public void DisableHUD() { HUD_Off(); }
        public void UpdateEnergy(int e) { UpdateEnergyText(e); }
        public void UpdateLevel(int l) { UpdateLevelText(l); }

        // Unity Events
        void Start()
        {
            music = GameManager.GetComponent<AudioSource>();
            music.volume = 0.07f;
            NormalMusicPitch = music.pitch;

            var overlaysAndHud = gameObject.GetComponentsInChildren<Canvas>(true);

            var overlayChildren = overlaysAndHud.Single(x => x.name == "Overlays")
                .gameObject.GetComponentsInChildren<Component>(true);
            foreach (var child in overlayChildren)
            {
                switch (child.name)
                {
                    case "Start":
                        startText = child.GetComponent<Text>();
                        break;
                    case "End":
                        endText = child.GetComponent<Text>();
                        break;
                    case "Menu":
                        menuText = child.GetComponent<Text>();
                        break;
                    case "BSOD":
                        bsod = child.gameObject;
                        break;
                    case "Background":
                        background = child.gameObject;
                        break;
                }
            }

            hud = overlaysAndHud.First(x => x.name == "HUD").gameObject;
            var hudChildren = hud.GetComponentsInChildren<Text>(true);
            foreach (var child in hudChildren)
            {
                switch (child.name)
                {
                    case "Points":
                        pointsText = child;
                        break;
                    case "FTL":
                        ftlText = child;
                        break;
                    case "Shoot":
                        shootText = child;
                        break;
                    case "LevelCenter":
                        levelCenterText = child;
                        break;
                    case "Level":
                        levelText = child;
                        break;
                }
            }
        }

        void Update()
        {
            if (startText.gameObject.activeSelf && Input.anyKeyDown)
            {
                CloseStartScreen();
                ExecuteEvents.Execute<IGameMessages>(GameManager, null, (x, _) => x.NextLevel());
            }

            if (endText.gameObject.activeSelf && Input.anyKeyDown) { Application.Quit(); }

            if (KeyCode.Escape._Pressed() || KeyCode.M._Pressed()) { ToggleMenu(); }

            if (menuText.gameObject.activeSelf && KeyCode.R._PressedWith(KeyCode.LeftShift))
            {
                ToggleMenu();
                ExecuteEvents.Execute<IPlayerMessages>(Player, null, (x, _) => x.Die());
            }
        }

        #region Overlays

        void ToggleMenu()
        {
            if (!menuText.gameObject.activeSelf)
            {
                music.pitch = 0.6f;
                Time.timeScale = 0.0001f;
                menuText.gameObject.SetActive(true);
                background.SetActive(true);
            }
            else
            {
                music.pitch = NormalMusicPitch;
                Time.timeScale = 1;
                menuText.gameObject.SetActive(false);
                background.SetActive(false);
            }
        }

        void ShowStartScreen()
        {
            startText.gameObject.SetActive(true);
            background.SetActive(true);
            hud.SetActive(false);
        }

        void CloseStartScreen()
        {
            startText.gameObject.SetActive(false);
            background.SetActive(false);
            hud.SetActive(true);
        }

        void ShowEndScreenScreen()
        {
            music.pitch = 0.4f;
            Time.timeScale = 0.1f;

            endText.gameObject.SetActive(true);
            background.SetActive(true);
            hud.SetActive(false);
        }

        void BSOD_On()
        {
            bsod.SetActive(true);
            background.SetActive(true);
        }

        void BSOD_Off()
        {
            bsod.SetActive(false);
            background.SetActive(false);
        }

        #endregion

        #region HUD

        void HUD_On()
        {
            hud.SetActive(true);
        }

        void HUD_Off()
        {
            hud.SetActive(false);
        }

        void UpdateEnergyText(int energy)
        {
            pointsText.text = "";
            for (var i = 0; i < energy; i++) { pointsText.text += '✦'; }
            for (var i = 0; i < 10 - energy; i++) { pointsText.text += '✧'; }
            ftlText.gameObject.SetActive(energy > 1);
            shootText.gameObject.SetActive(energy > 2);
        }

        void UpdateLevelText(int level)
        {
            levelText.text = "" + level;
            levelCenterText.text = "" + levelText.text;
            ShowLevelCenterText();
        }

        void ShowLevelCenterText()
        {
            var opacity = 0.7f;
            var speed = 1.4f;
            Action<float> setAlpha = a => levelCenterText.color = new Color(0, 255, 97, opacity * a);
            levelCenterText.gameObject.SetActive(true);

            Action<float> outF = ratio => setAlpha(1 - ratio);
            var fadeOut = Utils.MakeCoroutine(speed, outF, null);

            Action<float> inF = ratio => setAlpha(ratio);
            Action<float> inOutF = ratio => { if (ratio < 0.9f) inF(ratio); else StartCoroutine(fadeOut); };
            var fadeInOut = Utils.MakeCoroutine(speed, inOutF, () => { setAlpha(0); levelCenterText.gameObject.SetActive(false); });

            StartCoroutine(fadeInOut);
        }

        #endregion
    }

}
