using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lys
{
    public class Antiparticle : Seeker
    {
        new void Start()
        {
            base.Start();
            PositivelyCharged = false;
        }
    }
}
