using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lys.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Lys
{
    public class GameManager : MonoBehaviour, IGameMessages
    {
        public GameObject Player;
        public GameObject Background;
        public GameObject ParticlePF;
        public GameObject AntiparticlePF;
        public GameObject BlackholePF;

        private GameObject uiManager;
        private GameObject boundry;
        private LevelGenerator generator;
        private int currentLevel = 0;
        private System.Random rand = new System.Random();

        // Public Methods
        public int LevelNumber() { return currentLevel; }
        public void NextLevel() { LoadNextLevel(); }
        public void ReloadLevel() { GenerateLevel(); }
        public void Restart() { GoToStartLevel(); }

        // Unity Events
        void Start()
        {
            Cursor.visible = false;
            uiManager = GetComponentsInChildren<Component>(true).First(x => x.name == "UI").gameObject;
            boundry = Background.GetComponentsInChildren<Component>().First(x => x.name == "Boundry").gameObject;
            generator = new LevelGenerator(Player, boundry, AntiparticlePF, ParticlePF, BlackholePF);
        }

        #region Level

        void GenerateLevel()
        {
            ExecuteEvents.Execute<IUIMessages>(uiManager, null, (y, _) =>
            {
                y.UpdateLevel(currentLevel);
            });
            generator.Gen(currentLevel);
        }

        void GoToStartLevel()
        {
            currentLevel = 1;
            GenerateLevel();
        }

        void LoadNextLevel()
        {
            currentLevel++;
            if (currentLevel == 4) { GameEnded(); }
            else { GenerateLevel(); }
        }

        void GameEnded()
        {
            generator.Clear();

            ExecuteEvents.Execute<IUIMessages>(uiManager, null, (x, _) => x.ShowEnd());
            ExecuteEvents.Execute<IPlayerMessages>(Player, null, (x, _) => x.EndPose());

            var quit = Utils.MakeCoroutine(5f, (ratio) => { }, () => { Application.Quit(); });
            StartCoroutine(quit);
        }

        #endregion
    }
}
