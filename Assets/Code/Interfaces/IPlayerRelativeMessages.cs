using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Lys.Interfaces {
    public interface IPlayerRelativeMessages : UnityEngine.EventSystems.IEventSystemHandler
    {
        void SetPlayer(GameObject player);
    }
}