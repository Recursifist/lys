using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Lys.Interfaces {
    public interface IPlayerMessages : UnityEngine.EventSystems.IEventSystemHandler
    {
        float CurrentSize();
        void IncreaseSize();
        void DecreaseSize();
        void Die();
        void EndPose();
    }
}
