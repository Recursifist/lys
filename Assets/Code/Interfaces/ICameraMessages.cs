using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Lys.Interfaces
{
    public interface ICameraMessages : UnityEngine.EventSystems.IEventSystemHandler
    {
        void Zoom(float zoom);
        void Zoom(float zoom, float duration);
    }
}