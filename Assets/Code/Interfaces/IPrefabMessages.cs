using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Lys.Interfaces {
    public interface IPrefabMessages : UnityEngine.EventSystems.IEventSystemHandler
    {
        void SetSize(int size);
    }
}