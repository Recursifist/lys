using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Lys.Interfaces {
    public interface IUIMessages : UnityEngine.EventSystems.IEventSystemHandler
    {
        void ShowStart();
        void ShowEnd();
        void EnableBSOD();
        void DisableBSOD();
        void EnableHUD();
        void DisableHUD();
        void UpdateEnergy(int e);
        void UpdateLevel(int l);
    }
}
