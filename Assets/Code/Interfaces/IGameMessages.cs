using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Lys.Interfaces {
    public interface IGameMessages : UnityEngine.EventSystems.IEventSystemHandler
    {
        int LevelNumber();
        void ReloadLevel();
        void NextLevel();
        void Restart();
    }
}
