using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Lys.Interfaces;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Lys
{

    public class PlayerManager : Lightbody, IPlayerMessages
    {
        public GameObject GameManager;
        public GameObject FirePrefab;

        private GameObject uiManager;
        private Camera mainCam;
        private PlayerSizeHelper SH;
        private AudioSource touchSound;
        private AudioSource deathSound;
        private AudioSource hitSound;
        private AudioSource beginSound;
        private bool isDying = false;
        private bool ftlTimeout = false;
        private Light center;
        private float centerIntensity;
        private GameObject sphere;
        private Vector3 spherelocalScale;
        private float particleCameraVelocityScale;
        private float particleLengthScale;
        private System.Random rand = new System.Random();

        // Message Events
        public float CurrentSize() { return SH.S; }
        public void IncreaseSize() { IncreasePlayerSize(); }
        public void DecreaseSize() { DecreasePlayerSize(); }
        public void Die() { Death(); }
        public void EndPose() { PerformEndPose(); }

        #region Unity Events
        new void Start()
        {
            base.Start();
            SH = new PlayerSizeHelper();

            uiManager = GameObject.Find("UI");
            mainCam = GameObject.Find("Camera").GetComponent<Camera>();

            var audio = gameObject.GetComponentsInChildren<AudioSource>();
            touchSound = audio[0];
            hitSound = audio[1];
            deathSound = audio[2];
            beginSound = audio[3];

            center = GameObject.Find("Center").GetComponent<Light>();
            centerIntensity = center.intensity;
            sphere = GameObject.Find("Sphere");
            spherelocalScale = sphere.transform.localScale;

            particleCameraVelocityScale = ParticleRenderer.cameraVelocityScale;
            particleLengthScale = ParticleRenderer.lengthScale;

            UpdateSizing();
            Particles.Emit(9);
        }

        void Update()
        {
            if (isDying) return;

            StartAnimations();
            HandleMovement();

            // Testing
            if (Input.GetKeyDown(KeyCode.Equals)) IncreasePlayerSize();
            if (Input.GetKeyDown(KeyCode.Minus)) DecreasePlayerSize();
            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                ExecuteEvents.Execute<IGameMessages>(GameManager, null, (x, _) => x.NextLevel());
            }
        }

        #endregion

        #region Movement

        void HandleMovement()
        {
            var useMouse = false;
            var fromBottomLeft = mainCam.ScreenToViewportPoint(Input.mousePosition);

            var directionX = useMouse ? Math.Sign(fromBottomLeft.x - 0.5f) : Input.GetAxis("Horizontal");
            var directionY = useMouse ? Math.Sign(fromBottomLeft.y - 0.5f) : Input.GetAxis("Vertical");

            var speedX = SH.Speed * directionX;
            var speedY = SH.Speed * directionY;

            var ftlReady = SH.Energy > 1 && (directionX != 0 || directionY != 0) && !ftlTimeout;
            if (KeyCode.LeftShift._Pressed() && ftlReady)
            {
                EngageFTL();
            }
            else
            {
                rBody.velocity = new Vector2(speedX, speedY);
                gameObject.transform.Rotate(new Vector3(0, 0, directionX + directionY), Space.World);
            }

            if (KeyCode.Space._Pressed() && SH.Energy > 2)
            {
                FireParticle();
            }
        }

        void EngageFTL()
        {
            var directionX = Input.GetAxis("Horizontal");
            var directionY = Input.GetAxis("Vertical");
            var ftlX = SH.FTLSpeed * directionX;
            var ftlY = SH.FTLSpeed * directionY;
            ftlTimeout = true;

            var oLenScale = ParticleRenderer.lengthScale;
            ParticleRenderer.freeformStretching = true;
            deathSound.PlayOneShot(deathSound.clip, 1.5f);
            Time.timeScale = 0.6f;

            Action<float> smoothingF = ratio =>
            {
                var dimension = Mathf.Lerp(SH.Dimensions.x * 0.5f, SH.Dimensions.x, ratio);
                gameObject.transform.localScale = new Vector2()._SetSize(dimension);
                ParticleRenderer.lengthScale = 36f - (SH.S * 4);
                rBody.AddForce(new Vector2(ftlX, ftlY));
            };
            Action finalF = () =>
            {
                Time.timeScale = 1;
                gameObject.transform.localScale = SH.Dimensions;
                ParticleRenderer.freeformStretching = false;
                ParticleRenderer.lengthScale = oLenScale;
                Task.Delay(500).ContinueWith(_ => ftlTimeout = false);
            };

            StartCoroutine(Utils.MakeCoroutine(0.05f, smoothingF, finalF));
        }

        void FireParticle()
        {
            var directionX = Input.GetAxis("Horizontal");
            var directionY = Input.GetAxis("Vertical");
            var direction = new Vector2(directionX, directionY);

            var hits = Physics2D.CircleCastAll(transform.position, SH.Zoom * 2, Vector2.zero);
            var apHits = hits
                .Where(x => x.transform.CompareTag("Antiparticle"))
                .OrderBy(x => Vector2.Distance(transform.position, x.transform.position));
            if (apHits.Count() > 0)
            {
                var oLenScale = ParticleRenderer.lengthScale;
                ParticleRenderer.freeformStretching = true;
                ParticleRenderer.lengthScale = 10;
                ParticleRenderer.minParticleSize *= 2;
                Particles.Emit(40);
                DecreasePlayerSize();

                StartCoroutine(Utils.MakeDelayCoroutine(0.6f, () =>
                {
                    ParticleRenderer.lengthScale = oLenScale;
                    ParticleRenderer.freeformStretching = false;
                    ParticleRenderer.minParticleSize /= 2;

                    touchSound.PlayOneShot(touchSound.clip);
                    Destroy(apHits.First().transform.gameObject);
                }));
            }
        }

        #endregion

        #region Resizing

        void UpdateSizing()
        {
            gameObject.transform.localScale = SH.Dimensions;
            Trail.widthMultiplier = SH.TrailWidth;
            Trail.minVertexDistance = SH.S * 1.5f;
            Trail.time = 0.6f + (SH.S * 0.25f);

            var zoom = SH.Zoom + 0;
            ExecuteEvents.Execute<ICameraMessages>(mainCam.gameObject, null, (x, _) => x.Zoom(zoom));
            ExecuteEvents.Execute<IUIMessages>(uiManager, null, (x, _) => x.UpdateEnergy(SH.Energy));
        }

        void IncreasePlayerSize()
        {
            SH.ChangeSize(SH.S + PlayerSizeHelper.Load);
            UpdateSizing();
            Particles.Emit(30);
            touchSound.PlayOneShot(touchSound.clip);

            if (SH.Energy >= 10) LevelProgression();
        }

        void DecreasePlayerSize()
        {
            if ((SH.S - PlayerSizeHelper.Load) <= PlayerSizeHelper.Min)
            {
                deathSound.PlayOneShot(deathSound.clip);
                Die();
                return;
            }

            Particles.TriggerSubEmitter(0);
            SH.ChangeSize(SH.S - PlayerSizeHelper.Load);
            UpdateSizing();
            hitSound.PlayOneShot(hitSound.clip);
        }

        void Death()
        {
            isDying = true;
            rBody.velocity = Vector2.zero;
            rBody.Sleep();
            EngageFTL();
            ExecuteEvents.Execute<IUIMessages>(uiManager, null, (x, _) => x.EnableBSOD());


            Action<float> smoothingF = ratio =>
                {
                    var invertedRatio = 1 - ratio;
                    sphere.transform.Rotate(center.transform.up, 3);
                    center.intensity *= invertedRatio;
                    ParticleRenderer.cameraVelocityScale = 1f * ratio;
                };

            Action finalF = () =>
                {
                    ParticleRenderer.cameraVelocityScale = particleCameraVelocityScale;
                    ParticleRenderer.lengthScale = particleLengthScale;

                    PlayerReset();
                    ExecuteEvents.Execute<IGameMessages>(GameManager, null, (x, _) => x.Restart());
                    ExecuteEvents.Execute<IUIMessages>(uiManager, null, (x, _) => x.DisableBSOD());
                    rBody.WakeUp();
                };

            var playerDeath = Utils.MakeCoroutine(2f, smoothingF, finalF);
            StartCoroutine(playerDeath);
        }

        #endregion

        #region Level

        void LevelProgression()
        {
            PlayerReset();
            ExecuteEvents.Execute<IGameMessages>(GameManager, null, (x, _) => x.NextLevel());
        }

        void PlayerReset()
        {

            Time.timeScale = 1;
            Trail.enabled = false;
            isDying = false;
            ftlTimeout = false;

            SH.ChangeSize(PlayerSizeHelper.Min);
            UpdateSizing();
            center.intensity = centerIntensity;
            sphere.transform.localScale = spherelocalScale;

            gameObject.transform.position = Vector3.zero;

            Particles.Emit(6);
            Particles.TriggerSubEmitter(1);
            ParticleRenderer.cameraVelocityScale = particleCameraVelocityScale;
            ParticleRenderer.lengthScale = particleLengthScale;

            StartCoroutine(Utils.MakeDelayCoroutine(1f, () => Trail.enabled = true));

            ExecuteEvents.Execute<IUIMessages>(uiManager, null, (x, _) => x.DisableBSOD());
            beginSound.PlayOneShot(beginSound.clip);
        }

        void PerformEndPose()
        {
            touchSound.PlayOneShot(touchSound.clip);
            rBody.Sleep();
            Particles.Emit(12);
            Particles.TriggerSubEmitter(0);
            ExecuteEvents.Execute<ICameraMessages>(mainCam.gameObject, null, (x, _) => x.Zoom(0.7f));
        }

        #endregion

        #region Animation

        void StartAnimations()
        {
            RotateSphere();
            ChangeParticleColors();
        }

        void RotateSphere()
        {
            sphere.transform.Rotate(0.4f, 0.6f, 0.2f, Space.World);
        }

        void ChangeParticleColors()
        {
            var i = (int)Math.Min(Math.Max(Math.Abs(rBody.velocity.x), Math.Abs(rBody.velocity.y)), 32);
            ParticleMain.startSizeMultiplier = 6 + (i / 8);

            var colors = new[] { Color.white, Color.red, Color.yellow, Color.green, Color.cyan, Color.blue, Color.magenta };
            var c = colors[rand.Next(0, 6)];
            var pColor = new ParticleSystem.MinMaxGradient(new Color(c.r, c.g, c.b, 0.15f));
            ParticleMain.startColor = pColor;
        }

        #endregion
    }
}
