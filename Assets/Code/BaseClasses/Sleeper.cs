using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Lys.Interfaces;

namespace Lys
{

    public class Sleeper : MonoBehaviour
    {
        public bool Asleep;
        protected Rigidbody2D rBody;

        protected void Start() {
            rBody = GetComponent<Rigidbody2D>();
        }

        protected void OnBecameInvisible()
        {
            Asleep = true;
            if (rBody != null) { rBody.Sleep(); }
        }

        protected void OnBecameVisible()
        {
            Asleep = false;
            if (rBody != null) { rBody.WakeUp(); }
        }
    }
}