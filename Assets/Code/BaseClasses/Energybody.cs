using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Lys.Interfaces;

namespace Lys
{

    public class Energybody : Lightbody, IPlayerRelativeMessages
    {
        public GameObject Player;
        protected Collider2D PlayerCollider;
        protected Rigidbody2D PlayerRBody;
        protected bool PositivelyCharged;
        private bool destroyed;
        private System.Random rand = new System.Random();

        // Message Events
        public void SetPlayer(GameObject _player)
        {
            Player = _player;
        }

        // Unity Events
        new protected void Start()
        {
            base.Start();
            PlayerRBody = Player.GetComponent<Rigidbody2D>();
            PlayerCollider = Player.GetComponent<Collider2D>();
        }

        protected void Update()
        {
            if (!Asleep) Wander();
        }

        protected void OnCollisionEnter2D(Collision2D incoming)
        {
            if (destroyed) return;
            destroyed = true;
            Task.Delay(200).ContinueWith(_ => destroyed = false);
            
            var tag = incoming.gameObject.tag;
            switch (tag){
                case "Player":
                    if (PositivelyCharged)
                    {
                        ExecuteEvents.Execute<IPlayerMessages>(Player, null, (x, _) => x.IncreaseSize());
                        DieOut();
                    }
                    else
                    {
                        ExecuteEvents.Execute<IPlayerMessages>(Player, null, (x, _) => x.DecreaseSize());
                        DieOut();
                    }
                    break;
                default:
                    if(tag != gameObject.tag) DieOut();
                    break;
            }
        }

        // Methods
        void Wander()
        {
            var rPos = UnityEngine.Random.insideUnitCircle;
            var rFactor = rand.Next(3, 10);

            var wander = Utils.MakeCoroutine(1f,
                ratio =>
                {
                    var forceX = Mathf.Lerp(rPos.x, rPos.x * rFactor, ratio);
                    var forceY = Mathf.Lerp(rPos.y, rPos.y * rFactor, ratio);
                    rBody.AddForce(new Vector2(forceX, forceY));
                }, null);
            StartCoroutine(wander);
        }

        void DieOut()
        {
            var pRenderer = Particles.GetComponent<ParticleSystemRenderer>();

            var dieOut = Utils.MakeCoroutine(0.1f,
                ratio =>
                {
                    Particles.Emit(1);
                    var invertedRatio = 1 - ratio;
                    gameObject.transform.localScale *= invertedRatio;
                    pRenderer.cameraVelocityScale = 1f * ratio;
                },
                () => Destroy(gameObject));

            StartCoroutine(dieOut);
        }
    }
}