using System;
using System.Collections;
using System.Collections.Generic;
using Lys.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Lys
{

    public class Lightbody : Sleeper
    {
        protected ParticleSystem Particles;
        protected ParticleSystem.MainModule ParticleMain;
        protected ParticleSystemRenderer ParticleRenderer;
        protected ParticleSystem SubEmitter;
        protected TrailRenderer Trail;

        new protected void Start()
        {
            base.Start();

            Particles = GetComponent<ParticleSystem>();
            ParticleMain = Particles.main;
            ParticleRenderer = GetComponent<ParticleSystemRenderer>();
            if (Particles.subEmitters.subEmittersCount > 0)
            {
                SubEmitter = Particles.subEmitters.GetSubEmitterSystem(0);
            }

            Trail = GetComponent<TrailRenderer>();
        }
    }
}