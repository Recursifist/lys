using System.Drawing;
using System.Text.RegularExpressions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Lys.Interfaces;

namespace Lys
{
    public class Seeker : Energybody
    {
        protected float Range = 3;

        void LateUpdate()
        {
            if (!Asleep) ProximityAttack();
        }

        void ProximityAttack()
        {
            if (rBody.Distance(PlayerCollider).distance < Range)
            {
                var point = rBody.ClosestPoint(Player.transform.position);
                rBody.MovePosition(new Vector2(point.x, point.y));
            }
        }

    }
}
