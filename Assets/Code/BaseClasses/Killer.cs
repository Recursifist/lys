using System.Drawing;
using System.Text.RegularExpressions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Lys.Interfaces;

namespace Lys
{
    public class Killer : Sleeper, IPlayerRelativeMessages
    {
        protected GameObject Player;
        protected bool DoneKilling = false;

        public void SetPlayer(GameObject _player)
        {
            Player = _player;
        }

        protected new void Start()
        {
            base.Start();
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.tag == "Player" && ! DoneKilling)
            {
                ExecuteEvents.Execute<IPlayerMessages>(Player, null, (x, _) => x.Die());
                DoneKilling = true;
            }
        }
    }
}
