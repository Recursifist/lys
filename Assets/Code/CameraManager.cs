using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lys.Interfaces;
using UnityEngine;

namespace Lys
{
    public class CameraManager : MonoBehaviour, ICameraMessages
    {
        public GameObject Player;

        private Camera cam;

        // Message Events

        public void Zoom(float zoomTo)
        {
            SmoothZoom(zoomTo);
        }

        public void Zoom(float zoomTo, float duration)
        {
            SmoothZoom(zoomTo, duration);
        }

        // Unity Events

        void Start()
        {
            cam = gameObject.GetComponent<Camera>();
        }

        void Update()
        {
            TrackPlayer();
        }

        // Methods

        void TrackPlayer()
        {
            var targetPos = Player.transform.position;
            targetPos.z = -10;
            cam.transform.position = targetPos;
        }

        void SmoothZoom(float zoomTo, float duration = 1f)
        {
            Action<float> smoothingF = ratio =>
            {
                cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, zoomTo, ratio);
            };
            Action finalF = () => cam.orthographicSize = zoomTo;
            var smoothZoom = Utils.MakeCoroutine(duration, smoothingF, finalF);

            StartCoroutine(smoothZoom);
        }
    }
}
